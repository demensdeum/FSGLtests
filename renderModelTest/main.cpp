#include <ctime>
#include <iostream>
#include <memory>
#include <SDL2/SDL.h>
#include <FSGL/Controller/FSGLController.h>
#include <FlameSteelEngineGameToolkit/Data/Components/FSEGTFactory.h>
#include <FlameSteelEngineGameToolkitFSGL/Data/FSGTIOFSGLSystemFactory.h>

using namespace std;

int main()
{
	cout << "FSGL: Render Model Test" << endl;

    auto ioSystemParams = make_shared<FSEGTIOGenericSystemParams>();
    ioSystemParams->title = make_shared<string>("FSGL: Render Model Test");
    ioSystemParams->width = 640;
    ioSystemParams->height = 480;


	auto controller = make_shared<FSGLController>();
      	controller->initialize(ioSystemParams);

	auto object = FSEGTFactory::makeOnSceneObject(
														            make_shared<string>("Demensdeum Logo"),
														            make_shared<string>("Demensdeum Logo"),
														            shared_ptr<string>(),
														            make_shared<string>("data/models/skinnedCube/skinnedCube.fsglmodel"),
                                                                    shared_ptr<string>(),
														            0, 0, 0,
														            1, 1, 1,
														            0, 0, 0,
														            0);	

	auto materialLibrary = make_shared<MaterialLibrary>();

	auto graphicsObject = FSGTIOFSGLSystemFactory::graphicsObjectFrom(object, materialLibrary);

	if (graphicsObject.get() == nullptr) {
		throw logic_error("graphics object is null, test failed");
	}

	controller->addObject(graphicsObject);
    
    auto startTime = time(nullptr);
    
	SDL_Event event;
    while(time(nullptr) - startTime < 3) {
        SDL_PollEvent(&event);
        controller->render();    
    }

	return 0;
}
